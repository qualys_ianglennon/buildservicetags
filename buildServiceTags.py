import QualysAPI

import configparser
import xml.etree.ElementTree as ET
import csv
import sys


def createapi(user, passw, svr, proxy, enableProxy, debug):
    """Create a QualysAPI object for interaction with the Qualys API
    Returns the QualysAPI object"""

    api = QualysAPI.QualysAPI(svr = svr, usr=user, passwd=passw, proxy=proxy, enableProxy=enableProxy, debug=debug)

    return api


def readconfig(configfile='config.ini'):
    """Read configuration from file"""

    lconfig = configparser.ConfigParser()
    lconfig.read(filenames=configfile)
    return lconfig


def generatefindtag(tagname):
    """Create an XML ServiceRequest document to search for an Asset Tag based on its name
    Returns an XML document"""
    sr = ET.Element('ServiceRequest')
    filters = ET.SubElement(sr, 'filters')
    criteria = ET.SubElement(filters, 'Criteria', attrib={'field': "name", 'operator': "EQUALS"})
    criteria.text = tagname

    return sr


def generatetagdef(tagname, colour='#880000', ruletext=None, ruletype=None):
    """Create an XML Tag element for inclusion in an XML Service Request document
    Returns the XML Tag element"""
    tag = ET.Element('Tag')
    name = ET.SubElement(tag, 'name')
    name.text = tagname
    col = ET.SubElement(tag, 'color')
    col.text = colour
    if ruletype is not None:
        rtype = ET.SubElement(tag, 'ruleType')
        rtype.text = ruletype
        rtext = ET.SubElement(tag, 'ruleText')
        rtext.text = ruletext

    return tag


def generatesr(tagname, childdict:dict, parenttagid, colour='#880000'):
    """Creates an XML Service Request document to create an Asset Tag with child tags
    Returns the XML document"""

    sr = ET.Element('ServiceRequest')
    data = ET.SubElement(sr, 'data')
    tag = generatetagdef(tagname=tagname, colour=colour)
    parent = ET.SubElement(tag, 'parentTagId')
    parent.text = parenttagid
    children = ET.SubElement(tag, 'children')
    cset = ET.SubElement(children, 'set')

    for c in childdict.keys():
        child = generatetagdef(tagname=c, ruletype='NETWORK_RANGE', ruletext=','.join(childdict[c]))
        cset.append(child)

    data.append(tag)
    return sr


def deletetag(servicerequest, api:QualysAPI.QualysAPI):
    """Deletes an Asset Tag (and any child tags) from the Qualys subscription
    Returns a boolean value for success or failure"""
    url = '%s/qps/rest/2.0/delete/am/tag' % api.server
    response = api.makeCall(url=url, payload=ET.tostring(servicerequest, encoding='utf-8').decode())

    rcode = response.findall('.//responseCode')[0]
    if rcode.text == 'SUCCESS':
        return True
    else:
        print("%s" % ET.tostring(servicerequest, encoding='utf-8').decode())
        print("")
        print("%s" % ET.tostring(response, encoding='utf-8').decode())
        return False


def searchtag(servicerequest, api:QualysAPI.QualysAPI):
    url = '%s/qps/rest/2.0/search/am/tag' % api.server
    response = api.makeCall(url=url, payload=ET.tostring(servicerequest, encoding='utf-8').decode())

    rcode = response.findall('.//responseCode')[0]
    if rcode.text == 'SUCCESS':
        tagID = response.findall('.//id')
    else:
        tagID = -1

    return tagID


def createtag(servicerequest, api:QualysAPI.QualysAPI):
    url = '%s/qps/rest/2.0/create/am/tag' % api.server
    response = api.makeCall(url=url,payload=ET.tostring(servicerequest, encoding='utf-8').decode())

    rcode = response.findall('.//responseCode')[0]
    if rcode.text == 'SUCCESS':
        tagID = response.findall('.//id')[0]
    else:
        #print("%s" % ET.tostring(servicerequest, encoding='utf-8').decode())
        #print("")
        print("%s" % ET.tostring(response, encoding='utf-8').decode())
        tagID = -1

    return tagID


def parseInput(inputfile,config):

    apptypes = {}
    slines = {}
    slinetypes = {}
    scrits = {}
    sdatasecs = {}
    slineregions = {}
    slineenvs = {}

    assetipcol = int(config['DEFAULT']['AssetIPColumn'])-1
    apptypecol = int(config['DEFAULT']['AppTypeColumn'])-1
    slinecol = int(config['DEFAULT']['ServiceLineColumn'])-1
    slinetypecol = int(config['DEFAULT']['ServiceLineTypeColumn'])-1
    scritcol = int(config['DEFAULT']['ServiceCriticalityColumn'])-1
    sdataseccol = int(config['DEFAULT']['ServiceDataSecurityColumn'])-1
    slineregioncol = int(config['DEFAULT']['ServiceLineRegionColumn'])-1
    slineenvcol = int(config['DEFAULT']['ServiceLineEnvironmentColumn'])-1

    # Parse the CSV file
    with open(inputfile, newline='') as csvfile:
        hostreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in hostreader:
            asset = row[assetipcol]
            if asset == "" or asset == "UNKNOWN":
                continue

            apptype = row[apptypecol]
            if apptype == "":
                continue
            if apptype == "UNKNOWN":
                apptype = "UNKNOWN Application Type"
            if apptype not in apptypes.keys():
                apptypes[apptype] = [asset]
            else:
                apptypes[apptype].append(asset)

            sline = row[slinecol]
            if sline == "":
                continue
            if sline == "UNKNOWN":
                sline = "UNKNOWN Service Line"
            if sline not in slines.keys():
                slines[sline] = [asset]
            else:
                slines[sline].append(asset)

            slinetype = row[slinetypecol]
            if slinetype == "":
                continue
            if slinetype == "UNKNOWN":
                slinetype = "UNKNOWN Service Line Type"
            if slinetype not in slinetypes.keys():
                slinetypes[slinetype] = [asset]
            else:
                slinetypes[slinetype].append(asset)

            scrit = row[scritcol]
            if scrit == "":
                continue
            if scrit == "UNKNOWN":
                scrit = "UNKNOWN Service Criticality"
            if scrit not in scrits.keys():
                scrits[scrit] = [asset]
            else:
                scrits[scrit].append(asset)

            sdatasec = row[sdataseccol]
            if sdatasec == "":
                continue
            if sdatasec == "UNKNOWN":
                sdatasec = "UNKNOWN Security Classification"
            if sdatasec not in sdatasecs.keys():
                sdatasecs[sdatasec] = [asset]
            else:
                sdatasecs[sdatasec].append(asset)

            slineregion = row[slineregioncol]
            if slineregion == "":
                continue
            if slineregion == "UNKNOWN":
                slineregion = "UNKNOWN Service Line Region"
            if slineregion not in slineregions.keys():
                slineregions[slineregion] = [asset]
            else:
                slineregions[slineregion].append(asset)

            slineenv = row[slineenvcol]
            if slineenv == "":
                continue
            if slineenv == "UNKNOWN":
                slineenv = "UNKNOWN Service Line Environment"
            if slineenv not in slineenvs.keys():
                slineenvs[slineenv] = [asset]
            else:
                slineenvs[slineenv].append(asset)
    return (apptypes, slines, slinetypes, scrits, sdatasecs, slineregions, slineenvs)


def createtagtree(parentname, childdict:dict, api:QualysAPI.QualysAPI, parenttagid, testmode=False,
                  breakondelfail=True):

    if testmode:
        sr = generatefindtag(tagname=parentname)

        with open('test_%s_delete-criteria' % parentname, 'w') as output:
            output.write(ET.tostring(sr, encoding='utf-8').decode())

        sr = generatesr(tagname=parentname, childdict=childdict)
        with open('test_%s_createtag' % parentname, 'w') as output:
            output.write(ET.tostring(sr, encoding='utf-8').decode())
    else:
        sr = generatefindtag(tagname=parentname)
        if not deletetag(sr, api):
            if breakondelfail:
                print("Error deleting tag %s" % parentname)
                sys.exit(1)
        sr = generatesr(tagname=parentname, childdict=childdict, parenttagid=parenttagid)
        tagid = createtag(sr, api)

        if tagid == -1:
            print("Error creating tag tree %s" % parentname)
            sys.exit(2)

    return tagid


def evaluatetag(tagid, api:QualysAPI.QualysAPI):
    url = "%s/qps/rest/2.0/evaluate/am/tag/%s" % (api.server, tagid)
    api.makeCall(url=url)


if __name__ == '__main__':
    print("buildServiceTags: Startup")

    configfile = 'config.ini'
    config = readconfig(configfile)

    inputfile = config['DEFAULT']['InputFile']

    masterparent = config['DEFAULT']['TopLevelParent']

    apiserver = config['DEFAULT']['APIServer']
    apiuser = config['DEFAULT']['APIUser']
    apipass = config['DEFAULT']['APIPassword']
    proxy = config['DEFAULT']['ProxyAddress']

    if config['DEFAULT']['UseProxy'] == 'True' or config['DEFAULT']['UseProxy']  == 'Yes':
        enableproxy = True
    else:
        enableproxy = False

    if config['DEFAULT']['DebugOutput'] == 'True' or config['DEFAULT']['DebugOutput'] == 'Yes':
        enabledebug = True
    else:
        enabledebug = False

    if config['DEFAULT']['TestingMode'] == 'True' or config['DEFAULT']['TestingMode'] == 'Yes':
        enableTesting = True
    else:
        enableTesting = False

    if config['DEFAULT']['BreakOnDeleteFailure'] == 'True' or config['DEFAULT']['BreakOnDeleteFailure'] == 'Yes':
        breakondeletefailure = True
    else:
        breakondeletefailure = False

    api = createapi(user=apiuser, passw=apipass, svr=apiserver, proxy=proxy,
                    enableProxy=enableproxy, debug=enabledebug)

    (apptypes, slines, slinetypes, scrits, sdatasecs, slineregions, slineenvs) = parseInput(inputfile=inputfile, config=config)

    print("Generating Application Types")
    tagid = createtagtree(parentname='Application Types', childdict=apptypes, api=api, testmode=enableTesting,
                  breakondelfail=breakondeletefailure, parenttagid=masterparent)
    evaluatetag(tagid, api)

    print("Generating Service Lines")
    tagid = createtagtree(parentname='Service Lines', childdict=slines, api=api, testmode=enableTesting,
                  breakondelfail=breakondeletefailure, parenttagid=masterparent)
    evaluatetag(tagid, api)

    print("Generating Service Line Types")
    tagid = createtagtree(parentname='Service Line Types', childdict=slinetypes, api=api, testmode=enableTesting,
                  breakondelfail=breakondeletefailure, parenttagid=masterparent)
    evaluatetag(tagid, api)

    print("Generating Service Line Criticality Levels")
    tagid = createtagtree(parentname='Service Line Criticality Levels', childdict=scrits, api=api, testmode=enableTesting,
                  breakondelfail=breakondeletefailure, parenttagid=masterparent)
    evaluatetag(tagid, api)

    print("Generating Service Line Data Security Classifications")
    tagid = createtagtree(parentname='Service Line Data Security Classifications', childdict=sdatasecs, api=api,
                  testmode=enableTesting, breakondelfail=breakondeletefailure, parenttagid=masterparent)
    evaluatetag(tagid, api)

    print("Generating Service Line Regions")
    tagid = createtagtree(parentname='Service Line Regions', childdict=slineregions, api=api, testmode=enableTesting,
                  breakondelfail=breakondeletefailure, parenttagid=masterparent)
    evaluatetag(tagid, api)

    print("Generating Service Line Environments")
    tagid = createtagtree(parentname='Service Line Environments', childdict=slineenvs, api=api, testmode=enableTesting,
                  breakondelfail=breakondeletefailure, parenttagid=masterparent)
    evaluatetag(tagid, api)

    print("Complete!")
    sys.exit(0)
